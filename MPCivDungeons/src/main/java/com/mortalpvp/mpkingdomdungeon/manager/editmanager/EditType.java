package com.mortalpvp.mpkingdomdungeon.manager.editmanager;

public enum EditType {
    PORTLOCATIONS("port");

    private final String text;
    EditType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
