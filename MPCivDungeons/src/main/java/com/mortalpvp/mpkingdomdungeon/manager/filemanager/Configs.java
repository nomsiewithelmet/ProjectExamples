package com.mortalpvp.mpkingdomdungeon.manager.filemanager;

import java.util.HashMap;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public class Configs extends HashMap<String, Config> {

    private static Configs instance;

    public static Configs getInstance() {
        if (instance == null)
            instance = new Configs();
        return instance;
    }

    public Configs(){
        
    }


    public static void saveDefaultConfigs(boolean override){
        if (instance == null)
            getInstance();
        for (Entry<String, Config> conf : instance.entrySet()) {
            conf.getValue().saveDefaultConfig(override);
            conf.getValue().saveConfig();
        }
    }


}
