package com.mortalpvp.mpkingdomdungeon.manager.editmanager;

import com.mortalpvp.core.common.chat.C;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class EditManager {

    public void handleRequest(EditPlayer editPlayer, Player player, EditType type, WorldEditPlugin plugin){
        if(type.equals(EditType.PORTLOCATIONS)){
            Location a = plugin.getSelection(player).getMinimumPoint();
            Location b = plugin.getSelection(player).getMaximumPoint();
            List<Location> locs = new ArrayList<>();
            locs.add(a);
            locs.add(b);
            editPlayer.getDungeon().setPortlocations(editPlayer, locs);
            player.sendMessage(C.TAC("&cPort location set!"));

        }
    }
}
