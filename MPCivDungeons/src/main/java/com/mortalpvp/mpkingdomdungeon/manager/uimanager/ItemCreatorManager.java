package com.mortalpvp.mpkingdomdungeon.manager.uimanager;

import com.mortalpvp.core.api.Module;
import com.mortalpvp.core.api.MortalAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.UUID;

public class ItemCreatorManager extends Module {
    private HashMap<UUID, ItemStack> itemQueue = new HashMap<>();

    public ItemCreatorManager(MortalAPI api, String name) {
        super(api, name);
    }


    public HashMap<UUID, ItemStack> getItemQueue() {
        return itemQueue;
    }


    public boolean checkInQueue(Player p) {
        UUID uuid  = itemQueue.entrySet().stream().filter(uuidItemStackEntry ->
                uuidItemStackEntry.getKey().equals(p.getUniqueId())).findFirst().get().getKey();

        if(uuid.equals(p.getUniqueId())){
            return true;
        }

        return false;

    }


    public void ApplayChanges(Player player, ItemStack oldstack, ItemStack newstack){
        getItemQueue().remove(player.getUniqueId(), oldstack);
        getItemQueue().put(player.getUniqueId(), newstack);

    }

    public Player getFromQueue(ItemStack stack){
         return Bukkit.getPlayer(itemQueue.entrySet().stream().filter(uuidItemStackEntry ->
                uuidItemStackEntry.getValue().equals(stack)).findFirst().get().getKey());
    }

    public ItemStack getStackFromQueue(ItemStack stack){
        return itemQueue.entrySet().stream().filter(uuidItemStackEntry ->
                uuidItemStackEntry.getValue().equals(stack)).findFirst().get().getValue();
    }

    public ItemStack getStackFromQueue(Player player){
        return itemQueue.entrySet().stream().filter(uuidItemStackEntry ->
                uuidItemStackEntry.getKey().equals(player.getUniqueId())).findFirst().get().getValue();
    }

    public void removePlayer(Player p){
        getItemQueue().remove(p.getUniqueId());
    }

    public void removePlayer(ItemStack stack){
        getItemQueue().remove(itemQueue.entrySet().stream().filter(uuidItemStackEntry ->
                uuidItemStackEntry.getValue().equals(stack)).findFirst().get().getKey(), stack);
    }

}
