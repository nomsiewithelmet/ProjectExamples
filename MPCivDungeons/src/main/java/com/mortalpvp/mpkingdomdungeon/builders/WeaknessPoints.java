package com.mortalpvp.mpkingdomdungeon.builders;

import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

public class WeaknessPoints {
    private ItemStack item;
    private double extradamage;
    private Sound extraSound;

    public WeaknessPoints(ItemStack item, double extradamage, Sound extraSound) {
        this.item = item;
        this.extradamage = extradamage;
        this.extraSound = extraSound;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public double getExtradamage() {
        return extradamage;
    }

    public void setExtradamage(double extradamage) {
        this.extradamage = extradamage;
    }

    public Sound getExtraSound() {
        return extraSound;
    }

    public void setExtraSound(Sound extraSound) {
        this.extraSound = extraSound;
    }
}
