package com.mortalpvp.mpkingdomdungeon.command.commands;

import com.google.common.collect.Lists;
import com.mortalpvp.core.api.MortalAPI;
import com.mortalpvp.core.api.command.command.MortalCommand;
import com.mortalpvp.core.api.command.command.SubCommand;
import com.mortalpvp.core.api.command.data.Argument;
import com.mortalpvp.core.api.command.data.CommandArgument;
import com.mortalpvp.core.api.language.Translatable;
import com.mortalpvp.mpkingdomdungeon.Main;
import org.bukkit.command.CommandSender;

import java.util.List;

public class MobCreator extends MortalCommand<Main> {

    private final Main plugin;

    public MobCreator(Main plugin, MortalAPI api) {
        super(plugin, api, "civilization", "mob", new Translatable().append("Main command of the mob part from the kingdom dungeons"), "");

        this.plugin = plugin;
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public List<Argument> getArguments() {
        return null;
    }


    @Override
    public List<SubCommand> getSubCommands() {
        return Lists.newArrayList(
                new CreateSub(plugin, this)
        );
    }

    @Override
    public boolean onCommand(CommandSender commandSender, String s, List<CommandArgument> list) {
        return true;
    }

    @Override
    public void onError(CommandSender commandSender, Exception e) {

    }
}
