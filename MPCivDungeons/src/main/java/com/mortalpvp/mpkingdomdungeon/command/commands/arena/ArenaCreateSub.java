package com.mortalpvp.mpkingdomdungeon.command.commands.arena;

import com.google.common.collect.Lists;
import com.mortalpvp.core.api.command.command.MortalCommand;
import com.mortalpvp.core.api.command.command.SubCommand;
import com.mortalpvp.core.api.command.data.Argument;
import com.mortalpvp.core.api.command.data.ArgumentType;
import com.mortalpvp.core.api.command.data.CommandArgument;
import com.mortalpvp.core.api.language.Translatable;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ArenaCreateSub extends SubCommand {
    private final Main plugin;

    public ArenaCreateSub(Main plugin, MortalCommand baseCommand) {
        super(baseCommand, "create", new Translatable().append("blaad"), "");
        this.plugin = plugin;
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public List<Argument> getArguments() {
        return Lists.newArrayList(new Argument("name", new Translatable().append("Name of the dungeon"), true ,false,  ArgumentType.STRING));
    }

    @Override
    public List<SubCommand> getSubCommands() {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, String s, List<CommandArgument> list) {
        Player p = (Player) commandSender;
        if(plugin.getWorldEditPlugin().getSelection(p) != null){
            plugin.getDungeonManager().createDungeon(p.getWorld(), list.get(0).getData(), plugin.getWorldEditPlugin().getSelection(p).getMinimumPoint(), plugin.getWorldEditPlugin().getSelection(p).getMaximumPoint());
            p.sendMessage(C.TAC("&7[&aCREATED&7] &6Created a dungeon with the name: " + list.get(0).getData() ));

        }else{
            p.sendMessage(C.TAC("&7[&cERROR&7] &4No selection created with worldedit!"));
            return true;
        }

        return false;
    }

    @Override
    public void onError(CommandSender commandSender, Exception e) {

    }
}
