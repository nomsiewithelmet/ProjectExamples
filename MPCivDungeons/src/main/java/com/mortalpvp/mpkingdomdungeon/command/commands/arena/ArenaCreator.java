package com.mortalpvp.mpkingdomdungeon.command.commands.arena;

import com.google.common.collect.Lists;
import com.mortalpvp.core.api.MortalAPI;
import com.mortalpvp.core.api.command.command.MortalCommand;
import com.mortalpvp.core.api.command.command.SubCommand;
import com.mortalpvp.core.api.command.data.Argument;
import com.mortalpvp.core.api.command.data.CommandArgument;
import com.mortalpvp.core.api.language.Translatable;
import com.mortalpvp.mpkingdomdungeon.Main;
import org.bukkit.command.CommandSender;

import java.util.List;

public class ArenaCreator extends MortalCommand<Main> {
    private final Main plugin;

    public ArenaCreator(Main plugin, MortalAPI api) {
        super(plugin, api, "civilization", "arena", new Translatable().append("Main command for arena"), "");
        this.plugin = plugin;
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public List<Argument> getArguments() {
        return null;
    }

    @Override
    public List<SubCommand> getSubCommands() {
        return Lists.newArrayList(
                new ArenaCreateSub(plugin, this),
                new ArenaShowSub(plugin, this)
        );
    }

    @Override
    public boolean onCommand(CommandSender commandSender, String s, List<CommandArgument> list) {
        return false;
    }

    @Override
    public void onError(CommandSender commandSender, Exception e) {

    }
}
