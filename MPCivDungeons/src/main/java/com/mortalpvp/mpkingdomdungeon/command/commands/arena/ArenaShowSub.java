package com.mortalpvp.mpkingdomdungeon.command.commands.arena;

import com.mortalpvp.core.api.command.command.MortalCommand;
import com.mortalpvp.core.api.command.command.SubCommand;
import com.mortalpvp.core.api.command.data.Argument;
import com.mortalpvp.core.api.command.data.CommandArgument;
import com.mortalpvp.core.api.language.Translatable;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.ui.MainUI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ArenaShowSub extends SubCommand {

    private final Main plugin;
    public ArenaShowSub(Main plugin, MortalCommand baseCommand) {
        super(baseCommand, "show", new Translatable().append("blaad"), "");
        this.plugin = plugin;
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public List<Argument> getArguments() {

        return null;
    }

    @Override
    public List<SubCommand> getSubCommands() {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, String s, List<CommandArgument> list) {
        new MainUI(plugin).open((Player) commandSender);
        return false;
    }

    @Override
    public void onError(CommandSender commandSender, Exception e) {

    }
}
