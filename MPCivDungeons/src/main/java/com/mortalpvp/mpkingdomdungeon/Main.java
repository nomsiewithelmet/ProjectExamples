package com.mortalpvp.mpkingdomdungeon;

import com.mortalpvp.core.api.MortalAPI;
import com.mortalpvp.core.api.plugin.MortalPlugin;
import com.mortalpvp.mpkingdomdungeon.command.commands.MobCreator;
import com.mortalpvp.mpkingdomdungeon.command.commands.arena.ArenaCreator;
import com.mortalpvp.mpkingdomdungeon.listener.ChatListener;
import com.mortalpvp.mpkingdomdungeon.manager.DungeonManager;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditManager;
import com.mortalpvp.mpkingdomdungeon.manager.filemanager.Configs;
import com.mortalpvp.mpkingdomdungeon.manager.uimanager.ItemCreatorManager;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

public class Main extends MortalPlugin {
    private Configs fileManager;
    private DungeonManager dungeonManager;
    private EditManager editManager;
    private WorldEditPlugin worldEditPlugin;
    private ItemCreatorManager itemCreatorManager;
    private static Main singleton;
    @Override
    public void enable() {
        singleton = this;
        editManager = new EditManager();
        itemCreatorManager = new ItemCreatorManager(getMortalAPI(), "Item Creator Manager - v0.1");
        worldEditPlugin = WorldEditPlugin.getPlugin(WorldEditPlugin.class);
        fileManager = new Configs();
        dungeonManager = new DungeonManager(this.getMortalAPI(), "Dungeon Manager - v0.1");
        registerListeners(new ChatListener(this, getMortalAPI()));
        registerCommands(new MobCreator(this, getMortalAPI()), new ArenaCreator(this, getMortalAPI()));
    }

    @Override
    public void disable() {

    }

    public ItemCreatorManager getItemCreatorManager() {
        return itemCreatorManager;
    }

    public EditManager getEditManager() {
        return editManager;
    }

    public WorldEditPlugin getWorldEditPlugin() {
        return worldEditPlugin;
    }

    @Override
    public MortalAPI getMortalAPI() {
        return super.getMortalAPI();
    }

    public DungeonManager getDungeonManager() {
        return dungeonManager;
    }

    public Configs getFileManager() {
        return fileManager;
    }


    public static Main getInstance( ) {
        return singleton;
    }
}
