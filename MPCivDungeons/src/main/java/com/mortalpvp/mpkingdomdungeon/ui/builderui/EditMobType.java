package com.mortalpvp.mpkingdomdungeon.ui.builderui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.MobTypeBuilder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class EditMobType extends UI {
    public EditMobType(Main plugin, MobBuilder mobBuilder) {
        super(plugin, "Change mobtype", 9*6, InventoryType.CHEST);

        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i =  new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };
        setComponent(0, border);
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(8, border);
        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.ARROW).setDisplayName("&6Back").Create();
            }
    }.onInteract((player, clickType) -> {
        if (clickType.isLeftClick()) {
            new BuilderHome(mobBuilder, plugin).open(player);
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
        }
    }));        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(49, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, border);

        setComponent(20, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Skeleton").setDurability((short)3).setSkullOwner("MHF_Skeleton").Create();
            }
    }.onInteract((player, clickType) -> {
        if (clickType.isLeftClick()) {
            mobBuilder.setMobid(MobTypeBuilder.SKELETON.toString());
            new BuilderHome(mobBuilder, plugin).open(player);
            player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lSkeleton"));
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
        }
    }));


        setComponent(21, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6WitherSkeleton").setDurability((short)3).setSkullOwner("MHF_WSkeleton").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.WITHER_SKELETON.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lWitherSkeleton"));
            }
        }));

        setComponent(22, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Zombie").setDurability((short)3).setSkullOwner("MHF_Zombie").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.ZOMBIE.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lZombie"));
            }
        }));

        setComponent(23, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Creeper").setDurability((short)3).setSkullOwner("MHF_Creeper").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.CREEPER.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lCreeper"));
            }
        }));

        setComponent(24, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Enderman").setDurability((short)3).setSkullOwner("MHF_Enderman").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.ENDERMAN.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lEnderman"));
            }
        }));

        setComponent(29, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Silverfish").setDurability((short)3).setSkullOwner("metalhedd").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.SILVERFISH.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lSilverfish"));
            }
        }));
        setComponent(30, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Spider").setDurability((short)3).setSkullOwner("MHF_CaveSpider").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.SPIDER.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lSpider"));
            }
        }));
        setComponent(31, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Wolf").setDurability((short)3).setSkullOwner("MHF_Wolf").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.WOLF.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lWold"));

            }
        }));

        setComponent(32, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Ocelot").setDurability((short)3).setSkullOwner("MHF_Ocelot").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.OCELOT.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lOcelot"));

            }
        }));

        setComponent(33, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6Witch").setDurability((short)3).setSkullOwner("MHF_Witch").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                mobBuilder.setMobid(MobTypeBuilder.WITCH.toString());
                new BuilderHome(mobBuilder, plugin).open(player);
                player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed mob type to: &5&lWITCH"));
            }
        }));


    }
}
