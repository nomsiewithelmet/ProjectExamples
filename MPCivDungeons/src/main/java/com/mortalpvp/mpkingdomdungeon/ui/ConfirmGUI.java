package com.mortalpvp.mpkingdomdungeon.ui;

import com.mortalpvp.core.api.plugin.MortalPlugin;
import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class ConfirmGUI extends UI {
    public ConfirmGUI(MortalPlugin plugin, String name) {
        super(plugin, name, 9, InventoryType.CHEST);

        setComponent(3, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.STAINED_CLAY).setDurability((short) 5).setDisplayName("&aOK").Create();
            }
        }.onInteract((player, clickType) -> {
            player.sendMessage(C.TAC("&aConfirmed!"));
            close(player);
        }));

        setComponent(5, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.STAINED_CLAY).setDurability((short) 6).setDisplayName("&cNO").Create();
            }
        }.onInteract((player, clickType) -> {
            player.sendMessage(C.TAC("&cDeclined!"));
            close(player);
        }));

    }

}
