package com.mortalpvp.mpkingdomdungeon.ui.builderui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.WeaknessPoints;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.Spell;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class OverviewUI extends UI {
    public OverviewUI(Main plugin, MobBuilder mobBuilder) {
        super(plugin, "Overview", 9*6, InventoryType.CHEST);

        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i =  new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };

        setComponent(20, new UIComponent() {
                    @Override
                    public ItemStack getItemStack(Player player) {
                        if(mobBuilder.getMobid() == null || mobBuilder.getMobid().equalsIgnoreCase("")) {
                            return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6&lMobType").addLores("&7Mobtype: &cNothing").Create();
                        }else{
                            return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6&LMobType").addLores("&7Mobtype: &a" + mobBuilder.getMobid()).Create();
                        }
                    }
                });
        setComponent(22, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                if (mobBuilder.getLoot().isEmpty()) {
                    return new ItemCreator(Material.CHEST).setDisplayName("&6&lMob-Loot items").addLores("&cNo loot items").Create();
                } else {
                    StringBuilder b = new StringBuilder();
                    b.append("\n");
                    for (ItemStack stacks : mobBuilder.getLoot()) {
                        b.append(stacks.getItemMeta().getDisplayName() + "\n");
                    }
                    return new ItemCreator(Material.CHEST).setDisplayName("&6&lMob-Loot items").addLores("&7Added items: " + b.toString()).Create();
                }
            }
        });
        setComponent(24, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                if(mobBuilder.getSpells().isEmpty()) {
                    return new ItemCreator(Material.BLAZE_ROD).setDisplayName("&6&lSpells").addLores("&cNothing").Create();
                }else{
                    StringBuilder b = new StringBuilder();
                    b.append("\n");
                    for (Spell stacks : mobBuilder.getSpells()) {
                        b.append(stacks.getName() + "\n");
                    }
                    return new ItemCreator(Material.BLAZE_ROD).setDisplayName("&6&lSpells").addLores("&7Added items: " + b.toString()).Create();
                }
            }
        });

        setComponent(30, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.REDSTONE).setDisplayName("&6&lDefault Settings").addLores(new String[]{"&7Default settings:",
                "&7Follow range: &6&l" + mobBuilder.getMobDefaultSettings().getFollowrange(),
                "&7Mob Speed: &6&l" + mobBuilder.getMobDefaultSettings().getSpeed(),
                "&7DealthDamageOnHit: &6&l" + mobBuilder.getMobDefaultSettings().getDealthDamageonHit(),
                "&7Spining on atack: &6&l" + mobBuilder.getMobDefaultSettings().isSpinningOnHit()}).Create();
            }
        });

        setComponent(32, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                    return new ItemCreator(Material.DIAMOND_SWORD).setDisplayName("&6&lInventory").addLores(new String[]{"&7Applied items",
                    "&7Helmet: &6&l" + mobBuilder.getHelmet(),
                            "&7Chestplate: &6&l" + mobBuilder.getChestplate(),
                            "&7Leggings: &6&l" + mobBuilder.getLeggings(),
                            "&7Boots: &6&l" + mobBuilder.getBoots(),
                            "&7Atack-Item: &6&l" + mobBuilder.getSword()
                    }).Create();
                }
        });
        setComponent(40, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                if (mobBuilder.getWeaknesspoints().isEmpty()) {
                    return new ItemCreator(Material.POTION).setDisplayName("&6&lWeaknesses").setPotionBasePotionData(new PotionData(PotionType.WEAKNESS)).addItemFlags(ItemFlag.HIDE_ATTRIBUTES).addItemFlags(ItemFlag.HIDE_POTION_EFFECTS).addLores(new String[]{"&cNothing"}).Create();
                } else {
                    StringBuilder b = new StringBuilder();
                    b.append("\n");
                    for(WeaknessPoints points: mobBuilder.getWeaknesspoints()){
                        b.append("&7Extra-Damage: &6&l " + points.getExtradamage() + "&7- Extra sound: &6&l" + points.getExtraSound().name() + " &7- onItem: &7[&6&l" + points.getItem().getItemMeta().getDisplayName() + "&7] &6" +  points.getItem().getClass().getSimpleName() + "\n");
                    }
                    return new ItemCreator(Material.POTION).setDisplayName("&6&lWeaknesses").setPotionBasePotionData(new PotionData(PotionType.WEAKNESS)).addItemFlags(ItemFlag.HIDE_ATTRIBUTES).addItemFlags(ItemFlag.HIDE_POTION_EFFECTS).addLores(new String[]{"&7Applied weaknesses",b.toString()}).Create();

                }
            }
        });
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.ARROW).setDisplayName("&6Back").Create();
            }
        }.onInteract((player, clickType) -> {
            if(clickType.isLeftClick()){
                new BuilderHome(mobBuilder, plugin).open(player);
            }
        }));
        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, border);
        setComponent(49, border);
        setComponent(8, border);
        setComponent(0, border);
    }
}
