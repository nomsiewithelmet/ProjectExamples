package com.mortalpvp.mpkingdomdungeon.ui.builderui.itemcreator;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobDefaultSettings;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.BuilderHome;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.ItemCreatorUI;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.WichMenu;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class LootItemsUI extends UI {
    public LootItemsUI(Main plugin, MobBuilder mobBuilder) {

        super(plugin, "LootItems", 9 * 6, InventoryType.CHEST);
        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i = new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };
        startRefreshTask(10);
        setComponent(0, border);
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(8, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EMERALD_BLOCK).setDisplayName("&aAdd line").Create();
            }
        }.onInteract(((player, clickType) -> {
            if (clickType.isLeftClick()) {
                plugin.getItemCreatorManager().getItemQueue().put(player.getUniqueId(),
                        new ItemCreator(Material.PAPER).setDisplayName("Im a default item").Build());
                MobBuilder builder = new MobBuilder(new MobDefaultSettings(0.5, 0.2, false, 2));
                new ItemCreatorUI(builder, plugin.getItemCreatorManager().getStackFromQueue(player), WichMenu.LOOTITEMS,
                        plugin).open(player);
            }
        })));
        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.ARROW).setDisplayName("&6Back").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new BuilderHome(mobBuilder, plugin).open(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
            }
        }));
        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(49, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, border);
        mobBuilder.getLoot().forEach(item -> addComponent(new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemCreator creator = new ItemCreator(item.getType())
                        .setDisplayName(item.getItemMeta().getDisplayName())
                        .addLores(item.getItemMeta().getLore().toArray(new String[0]));
                item.getItemMeta().getEnchants().forEach((creator::addEnchantment));
                return creator.Create();
            }
        }));

    }
}
