package xyz.herex.sprookjescraft.hotel.hotelsc.utils;

import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class ItemBuilder {

    private ItemStack stack;
    private String displayname = ""; // Unused
    private String[] lore; // Unused
    private Material m; // < De variable gewoon 'material' noemen


    // Mooi gebruik van fluent API
    // Misschien heb je dit onbewust gedaan, maar je hebt een simpele variant van het
    // Builder design pattern uitgewerkt - https://sourcemaking.com/design_patterns/builder
    // Design patterns zijn herbruikbare oplossingen voor veel voorkomende problemen.
    public ItemBuilder(Material material){
        this.m = material;
        stack = new ItemStack(material);
    }

    public ItemBuilder withDisplayname(String input){
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(C.K(input));
        stack.setItemMeta(meta);
        return this;
    }

    public ItemBuilder withLore(String[] input){
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = new ArrayList<>();
        Arrays.stream(input).forEach(s -> lore.add(C.K(s))); // Mooie lambda oplossing
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return this;
    }

    // Ik raad je aan om de size hier van het type 'int' aan te houden
    public ItemBuilder withData(byte size){
        // En vervolgens de cast (conversie) van int naar byte hier uitvoeren
        // stack.setDurability((byte) size);
        stack.setDurability(size);
        return this;
    }
    // Aangezien ik zie dat al je withData aanroepers al een int-naar-byte cast uitvoeren en dat maakt de regel daar onnodig veel langer

    public ItemStack build(){
        return stack;
    }
}
