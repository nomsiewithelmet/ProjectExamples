package xyz.herex.sprookjescraft.hotel.hotelsc.filemanager;

import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class Config {

    private String fileName;
    private File configFile;
    private FileConfiguration fileConfiguration;
    /**
     * Constructor
     *
     * @param name name for the config
     */
    public Config(String name) {
        this(name, HotelSC.getInstance().getDataFolder());
    }

    /**
     * Constructor
     *
     * @param name   name for the config
     * @param folder the folder you want to add it in.
     */
    public Config(String name, File folder) {
        if (HotelSC.getInstance() == null)
            throw new IllegalArgumentException("plugin cannot be null!");
        this.fileName = name + (name.endsWith(".yml") ? "" : ".yml");
        if (folder == null)
            throw new IllegalStateException();
        this.configFile = new File(folder, fileName);
    }

    /**
     * Reload the config
     */
    public void reloadConfig() {
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

        InputStream defConfigStream = HotelSC.getInstance().getResource(fileName);
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
            fileConfiguration.setDefaults(defConfig);
        }
    }

    /**
     * Get the configuration in the config
     *
     * @return the FileConfiguration of the config
     */
    public FileConfiguration getConfig() {
        if (fileConfiguration == null)
            reloadConfig();
        return fileConfiguration;
    }

    /**
     * Save the config
     */
    public void saveConfig() {
        if (fileConfiguration != null && configFile != null)
            try {
                getConfig().save(configFile);
            } catch (IOException ex) {
                HotelSC.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
            }
    }

    /**
     * Save the default config
     */
    void saveDefaultConfig() {
        saveDefaultConfig(false);
    }

    /**
     * Save the default config
     *
     * @param override if the existing config should be overriden
     */
    void saveDefaultConfig(boolean override) {
        if (!configFile.exists())
            HotelSC.getInstance().saveResource(fileName, override);
    }
}
