package xyz.herex.sprookjescraft.hotel.hotelsc.menu;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */

// Het hebben van deze enum is oké, maar je zou ook deze enum kunnen weghalen en werken met InventoryRows ( * 9 )
public enum InventorySize {
    NINE(9),EIGHTEEN(18), TWENTYSEVEN(27), THIRTYSIX(36), FORTYFIVE(45), FIFTYFOUR(54);

    private final int value;

    private InventorySize(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
