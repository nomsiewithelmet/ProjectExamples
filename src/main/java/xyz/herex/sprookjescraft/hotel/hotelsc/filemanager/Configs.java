package xyz.herex.sprookjescraft.hotel.hotelsc.filemanager;

import java.util.HashMap;

/**
 * Waarom deze klasse? Ben je van plan om de saveDefaultConfigs nog te gaan gebruiken?
 * Zo niet, YAGNI!
 * http://www.extremeprogramming.org/rules/early.html
 */

public class Configs extends HashMap<String, Config> {

    private static Configs instance;

    public static Configs getInstance() {
        if (instance == null)
            instance = new Configs();
        return instance;
    }


    public static void saveDefaultConfigs(boolean override){
        if (instance == null)
            getInstance();
        for (Entry<String, Config> conf : instance.entrySet()) {
            conf.getValue().saveDefaultConfig(override);
            conf.getValue().saveConfig();
        }
    }


}
