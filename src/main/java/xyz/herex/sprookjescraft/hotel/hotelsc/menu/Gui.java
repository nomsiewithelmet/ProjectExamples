package xyz.herex.sprookjescraft.hotel.hotelsc.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.ItemBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Hier een mini lesje abstractie.
 * Wil je forceren dat externe code alleen mag werken met RoomMenu en ConfirmMenu, en niet direct een Gui object mag
 * maken? Dat je wil voorkomen dat de Command klasse ineens dit gaat doen:
 * Gui gui = new Gui("RoomMenu", ...);
 * In plaats van:
 * Gui gui = new RoomMenu();
 *
 * Als je alleen wil dat RoomMenu en ConfirmMenu gemaakt kunnen worden, dan moet je deze klasse abstract maken:
 *
 * public abstract class Gui {
 *
 * En ga dan maar eens dit proberen in je Command klasse:
 * Gui gui = new Gui("RoomMenu", ...);
 */

public class Gui {

    private String name;
    private InventorySize size;
    private List<GuiComponent> items = new ArrayList<>();
    private GuiType guiType;
    private InventoryHolder holder;
    int page = 1;

    public Gui(String name, InventorySize size, GuiType guiType, InventoryHolder holder) {
        this.name = name;
        this.size = size;
        this.guiType = guiType;
        this.holder = holder;
    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void addItem(int position, ItemStack stack, BiConsumer<Player, ClickType> bi){
        items.add(new GuiComponent(position, stack, bi));
    }

    public void addItem(ItemStack stack){
        items.add(new GuiComponent(- 1, stack));
    }


    public void addItem(ItemStack stack, BiConsumer<Player, ClickType> bi){
        items.add(new GuiComponent(-1, stack, bi));
    }
    public void addItem(int position, ItemStack stack){
        items.add(new GuiComponent(position, stack));
    }
    public InventoryHolder getHolder() {
        return holder;
    }

    public List<GuiComponent> getItems() {
        return items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InventorySize getSize() {
        return size;
    }

    public void setSize(InventorySize size) {
        this.size = size;
    }

    public GuiType getGuiType() {
        return guiType;
    }

    public void setGuiType(GuiType guiType) {
        this.guiType = guiType;
    }

    // Van mij mogen persoonlijk de simpele getters en setters onderaan staan en 'open' verder omhoog
    public void open(Player player){


        Inventory inv;
        if(holder != null){
            inv = Bukkit.createInventory(holder, size.getValue(), name);
        }else {
            inv = Bukkit.createInventory(null, size.getValue(), name); // Deze else hoeft niet
        }
        for(GuiComponent component : items){
            if(component.location == -1){
                inv.addItem(component.getItemStack());
            }else
            inv.setItem(component.getLocation(), component.getItemStack());
        }

        player.openInventory(inv);
    }
}
