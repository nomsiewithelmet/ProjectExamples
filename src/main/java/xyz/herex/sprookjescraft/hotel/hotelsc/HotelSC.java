package xyz.herex.sprookjescraft.hotel.hotelsc;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.herex.sprookjescraft.hotel.hotelsc.command.Command;
import xyz.herex.sprookjescraft.hotel.hotelsc.events.PlayerEvents;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.GuiManager;
import xyz.herex.sprookjescraft.hotel.hotelsc.room.RoomManager;

public final class HotelSC extends JavaPlugin {
    
    private static HotelSC instance;
    private RoomManager roomManager;
    private WorldEditPlugin worldEditPlugin;
    private GuiManager guiManager;
    @Override
    public void onEnable() {
        instance = this;
        worldEditPlugin = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        roomManager = new RoomManager();
        guiManager = new GuiManager();
        getCommand("room").setExecutor(new Command());
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerEvents(), this);

    }


    @Override
    public void onDisable() {
    }

    public GuiManager getGuiManager() {
        return guiManager;
    }

    public WorldEditPlugin getWorldEditPlugin() {
        return worldEditPlugin;
    }

    public RoomManager getRoomManager() {
        return roomManager;
    }

    public static HotelSC getInstance() {
        return instance;
    }
}
