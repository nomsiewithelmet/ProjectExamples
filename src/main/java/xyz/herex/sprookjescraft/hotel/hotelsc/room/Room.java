package xyz.herex.sprookjescraft.hotel.hotelsc.room;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class Room {
    // Uitstekend gebruik van OOP (Object Oriented Programming): Je hebt een publieke klasse met daarin afgeschermde, private variabelen die via getters en setters aan te roepen zijn.
    // Zo kan je later extra logica toevoegen in de getters of setters hier, zonder andere code buiten deze klasse aan te hoeven passen.
    private String owner;
    private String roomLabel;
    private Location[] cornerLocations;
    private boolean rented;
    private List<String> invitedplayers;

    // boolean is een primitief type. Dat is goed om te gebruiken, maar houd altijd in je achterhoofd
    // dat je niet kan verwijzen naar primitieve types. De waarde wordt altijd gekopiëerd. Voorbeeldje:
    /**
     * boolean rented = true;
     * boolean anotherVariable = rented;
     *
     * rented = false;
     */
    // In dit geval is anotherVariable nog true (!)
    // Als je wil weten hoe dit precies komt, vraag het dan aan mij, dan kan ik je de werking van stack & heap memory uitleggen.

    public Room(String owner, String roomLabel, Location[] cornerLocations, boolean rented, List<String> invitedplayers) {
        this.owner = owner;
        this.roomLabel = roomLabel;
        this.cornerLocations = cornerLocations;
        this.rented = rented;
        this.invitedplayers = invitedplayers;
    }

    public List<String> getInvitedplayers() {
        return invitedplayers;
    } // Naming convention (players = Players)

    public String getOwner() {
        return owner;
    }


    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRoomLabel() {
        return roomLabel;
    }

    /**
     * YAGNI principe is niet van toepassing op kleine getters en setters
     * Dus deze ongebruikte methodes kan je laten staan
     * @see RoomManager
     */

    public void setRoomLabel(String roomLabel) {
        this.roomLabel = roomLabel;
    }

    public Location[] getCornerLocations() {
        return cornerLocations;
    }

    public void setCornerLocations(Location[] cornerLocations) {
        this.cornerLocations = cornerLocations;
    }

    public boolean isRented() {
        return rented;
    }

    public void setRented(boolean rented) {
        this.rented = rented;
    }
}
