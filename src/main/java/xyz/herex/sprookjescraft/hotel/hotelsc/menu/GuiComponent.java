package xyz.herex.sprookjescraft.hotel.hotelsc.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.function.BiConsumer;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class GuiComponent {

    public int location; // Deze moet private zijn
    private ItemStack itemStack;
    private BiConsumer<Player, ClickType> onClick;


    public GuiComponent(int location, ItemStack itemStack, BiConsumer<Player, ClickType> onClick) {
        this.itemStack = itemStack;
        this.onClick = onClick;
        this.location = location;
    }

    public GuiComponent(int location, ItemStack itemStack) {
        this.itemStack = itemStack;
        this.location = location;
    }

    public GuiComponent(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public BiConsumer<Player, ClickType> getOnClick() {
        return onClick;
    }

    public void setOnClick(BiConsumer<Player, ClickType> onClick) {
        this.onClick = onClick;
    }
}
