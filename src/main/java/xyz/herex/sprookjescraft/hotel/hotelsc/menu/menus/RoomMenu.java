package xyz.herex.sprookjescraft.hotel.hotelsc.menu.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import sun.applet.Main;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Config;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Configs;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.Gui;

import xyz.herex.sprookjescraft.hotel.hotelsc.menu.GuiManager;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.GuiType;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.InventorySize;
import xyz.herex.sprookjescraft.hotel.hotelsc.room.Room;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.ItemBuilder;

import java.util.UUID;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class RoomMenu extends Gui {

    /**
     * De ItemBuilder.withData() methode kan handiger zodat je hier (byte) weg kan halen
     * @see ItemBuilder
     */
    public RoomMenu() {
        super("RoomMenu", InventorySize.NINE, GuiType.NORMAL, null);
        for(Room r : HotelSC.getInstance().getRoomManager().getRooms()){
            if(r.getCornerLocations() == null && !r.isRented()) {
                addItem(new ItemBuilder(Material.STAINED_CLAY).withDisplayname("&f " + r.getRoomLabel() + " &f- &7Mist locatie").withLore(new String[]{"&cMist de hotel locaties!"}).withData((byte) 9).build(), (player, clickType) -> {
                    player.closeInventory();
                });
            }else if(!r.isRented()){
                addItem(new ItemBuilder(Material.STAINED_CLAY).withDisplayname("&f " + r.getRoomLabel() + " &f- &aVrij").withLore(new String[]{}).withData((byte)5).build());
            }
            if(r.isRented()) {
                addItem(new ItemBuilder(Material.STAINED_CLAY).withDisplayname("&f " + r.getRoomLabel() + " &f- &cVerhuurd").withLore(new String[]{"&9Verhuurd aan: " + Bukkit.getOfflinePlayer(r.getOwner()).getName(), "", "&cKlik op mij als je de huur wilt beeindigen!"}).withData((byte) 14).build(), (player, clickType) -> {
                    HotelSC.getInstance().getRoomManager().getConfirmcache().put(player.getUniqueId(), r);
                    Gui gui = new ConfirmMenu();
                    gui.open(player);
                });
            }
        }
    }
}
