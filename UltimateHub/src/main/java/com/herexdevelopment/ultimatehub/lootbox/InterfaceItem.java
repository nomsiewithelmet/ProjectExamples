package com.herexdevelopment.ultimatehub.lootbox;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 2-5-18
 */
public class InterfaceItem {

    private int id, slotID;
    private String displayname,event;

    public InterfaceItem(int id, int slotID, String displayname, String event) {
        this.id = id;
        this.slotID = slotID;
        this.displayname = displayname;
        this.event = event;
    }

    public int getId() {
        return id;
    }

    public int getSlotID() {
        return slotID;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getEvent() {
        return event;
    }
}
