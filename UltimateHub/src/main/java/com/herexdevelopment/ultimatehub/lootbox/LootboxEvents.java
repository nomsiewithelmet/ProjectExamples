package com.herexdevelopment.ultimatehub.lootbox;

import com.herexdevelopment.ultimatehub.particle.ParticleDesign;
import com.herexdevelopment.ultimatehub.particle.ParticleManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;
import com.herexdevelopment.ultimatehub.utils.partcile.ParticleEffect;


/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 1-5-18
 */
public class LootboxEvents {
    private static LootboxEvents instance = null;
    public LootboxEvents() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static LootboxEvents getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }



        public void execute(String event, LootBox b) {
            for (String key : ConfigManager.getInstance().getConfig("data").getConfiguration().getConfigurationSection("crates").getKeys(false)) {
                if (key.contains(b.getCrateName())) {
                    World wl = Bukkit.getServer().getWorld(ConfigManager.getInstance().getConfig("data").getConfiguration().getString("crates." + key + ".world"));
                    double xl = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locx");
                    double yl = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locy");
                    double zl = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locz");
                    String stripped = event.replace("{", "").replace("}", "");
                    String[] args = stripped.split(":");
                    switch (args[0]) {
                        case "particle":
                            String[] cords = args[3].split(",");
                            ParticleDesign design = ParticleManager.getInstance().getParticle(args[2]);
                            float x = Float.parseFloat(cords[0]);
                            float y = Float.parseFloat(cords[1]);
                            float z = Float.parseFloat(cords[2]);
                            if(args[2].equalsIgnoreCase("helix")) {
                                return;
                            }
                            ParticleEffect.fromName(args[1]).display((float)design.getX(), (float)design.getY(),(float) design.getZ(),(float)design.getSpeed(), design.getAmount(), new Location(wl, xl, yl, zl).add(x + 0.5, y, z + 0.5), 500);
                            break;

                            case "sound":
                            wl.playSound(new Location(wl,xl,yl,zl),Sound.valueOf(args[1]), 1,1);
                            break;
                        default:
                            break;
                    }
                }
            }
        }







    public void execute(Location loc, String event, LootBox b) {
        for (String key : ConfigManager.getInstance().getConfig("data").getConfiguration().getConfigurationSection("crates").getKeys(false)) {
            if (key.contains(b.getCrateName())) {
                String stripped = event.replace("{", "").replace("}", "");
                String[] args = stripped.split(":");
                switch (args[0]) {
                    case "particle":
                        Location defaultloc = loc;
                        String[] cords = args[3].split(",");
                        float x = Float.parseFloat(cords[0]);
                        float y = Float.parseFloat(cords[1]);
                        float z = Float.parseFloat(cords[2]);
                        ParticleDesign design = ParticleManager.getInstance().getParticle(args[2]);
                        ParticleEffect.fromName(args[1]).display((float) design.getX(), (float)design.getY(), (float)design.getZ(),(float)design.getSpeed(), design.getAmount(), new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ()).add(x + 0.5, y, z + 0.5), 500);
                        break;
                    //http://www.theredengineer.com/1.9-playsound-list.html
                    case "sound":
                        loc.getWorld().playSound(loc,Sound.valueOf(args[1]), 1,1);
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
