package com.herexdevelopment.ultimatehub.lootbox;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;
import com.herexdevelopment.ultimatehub.UltimateHub;

import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class LootBox {

    private String crateName;
    private int crateBlockId, invSize;
    private List<String> hologramlines, effectevent, idleEffect;
    private List<LootItem> lootitems;
    private List<InterfaceItem> interfaceItems;
    private List<Location> locs;

    public LootBox(int invSize, String crate_name, int crateBlockId, List<String> hologramlines, List<String> effectevent, List<String> idle_effect, List<LootItem> lootitems, List<InterfaceItem> interfaceItems, List<Location> locs) {
        this.invSize = invSize;
        this.locs = locs;
        this.crateName = crate_name;
        this.crateBlockId = crateBlockId;
        this.hologramlines = hologramlines;
        this.effectevent = effectevent;
        this.idleEffect = idle_effect;
        this.lootitems = lootitems;
        this.interfaceItems = interfaceItems;
        startIdleEffects();
    }

    public int getInvSize() {
        return invSize;
    }

    public String getCrateName() {
        return crateName;
    }

    public int getCrateBlockId() {
        return crateBlockId;
    }

    public List<String> getHologramlines() {
        return hologramlines;
    }

    public List<String> getEffectevent() {
        return effectevent;
    }

    public List<String> getIdleEffect() {
        return idleEffect;
    }

    public List<LootItem> getLootitems() {
        return lootitems;
    }

    public List<InterfaceItem> getInterfaceItems() {
        return interfaceItems;
    }

    public List<Location> getLocs() {
        return locs;
    }


    public BukkitTask startIdleEffects() {
        long pauseTime = 20L;
        long totalTime = getTotalTime() + pauseTime;
        return Bukkit.getScheduler().runTaskTimer(UltimateHub.getInstance(), () -> {
            // per effect:
            for (String s : getIdleEffect()) {
                String[] args = s.replace("{", "").replace("}", "").split(":");
                if (args[0].equalsIgnoreCase("particle")) {
                    int interval = Integer.parseInt(args[4].replace("[", "").replace("]", ""));
                    Bukkit.getScheduler().runTaskLater(UltimateHub.getInstance(), () -> {
                        LootboxEvents.getInstance().execute(s, this);
                    }, interval * 20L);
                }else if(args[0].equalsIgnoreCase("sound")){
                    int interval = Integer.parseInt(args[2].replace("[", "").replace("]", ""));
                    Bukkit.getScheduler().runTaskLater(UltimateHub.getInstance(), () -> {
                        LootboxEvents.getInstance().execute(s, this);
                    }, interval * 20L);
                }
            }
        }, 0L, totalTime);

    }





    public void startPlayEffect(Location loc) {
            for (String s : getEffectevent()) {
                String[] args = s.replace("{", "").replace("}", "").split(":");
                if (args[0].equalsIgnoreCase("particle")) {
                    int interval = Integer.parseInt(args[4].replace("[", "").replace("]", ""));
                    Bukkit.getScheduler().runTaskLater(UltimateHub.getInstance(), () -> {
                        LootboxEvents.getInstance().execute(loc, s, this);
                    }, interval * 20L);
                } else if (args[0].equalsIgnoreCase("sound")) {
                    int interval = Integer.parseInt(args[2].replace("[", "").replace("]", ""));
                    Bukkit.getScheduler().runTaskLater(UltimateHub.getInstance(), () -> {
                        LootboxEvents.getInstance().execute(loc, s, this);
                    }, interval * 20L);
                }

            }



    }
    private int getTotalTime(){
        int i = 0;
        for(String item : getIdleEffect()) {
            String[] args = item.replace("{", "").replace("}", "").split(":");
            if (args[1].equalsIgnoreCase("particle")) {
                int interval;
                interval = Integer.parseInt(args[3].replace("[", "").replace("]", ""));
                i = i + interval;
            }
        }

        return i;
    }

}
