package com.herexdevelopment.ultimatehub.utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class UltimateHologram {


    private static List<UltimateHologram> holos = new ArrayList<UltimateHologram>();

    private ArmorStand hologram;
    private String text;
    private Location loc;

    public ArmorStand getHologram() {
        return hologram;
    }

    public String getText() {
        return text;
    }

    public Location getLocation() {
        return loc;
    }

    public UltimateHologram(Location loc, String text) {
        this.loc = loc;
        this.text = text;
    }

    public void create(){
        hologram = loc.getWorld().spawn(loc.add(0.3,0,0.5), ArmorStand.class);
        hologram.setCustomName(ChatColor.translateAlternateColorCodes('&', text));
        hologram.setCustomNameVisible(true);
        hologram.setGlowing(false);
        hologram.setRemainingAir(0);
        hologram.setVisible(false);

        holos.add(this);
    }

    public void remove(){
        hologram.remove();
        holos.remove(this);
    }

    public void teleport(Location loc){
        hologram.teleport(loc);
    }

    public void changeText(String text){
        hologram.setCustomName(text);
    }

    public static UltimateHologram getByLocation(Location loc){
        for(UltimateHologram holo : UltimateHologram.holos){
            if(holo.getLocation().distance(loc) <= 3){
                return holo;
            }
        }
        return null;
    }

}
