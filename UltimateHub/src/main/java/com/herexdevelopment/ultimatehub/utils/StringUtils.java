package com.herexdevelopment.ultimatehub.utils;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class StringUtils {

    public static String repColor(String input){
        return ChatColor.translateAlternateColorCodes('&', input);

    }

    public static String repvar(Player p, String input){
        return PlaceholderAPI.setPlaceholders(p, input);
    }
}
