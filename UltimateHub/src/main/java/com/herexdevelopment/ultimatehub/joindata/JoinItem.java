package com.herexdevelopment.ultimatehub.joindata;

import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class JoinItem {
    private int itemid,sound_effect, slot_id;
    private String displayname,menuname;
    private List<String> lore;

    public JoinItem(int itemid, int slot_id,  int sound_effect, String displayname, String menuname, List<String> lore) {
        this.itemid = itemid;
        this.sound_effect = sound_effect;
        this.displayname = displayname;
        this.menuname = menuname;
        this.lore = lore;
        this.slot_id = slot_id;
    }

    public int getSlot_id() {
        return slot_id;
    }

    public int getItemid() {
        return itemid;
    }


    public String getDisplayname() {
        return displayname;
    }

    public String getMenuname() {
        return menuname;
    }

    public List<String> getLore() {
        return lore;
    }

    public int getSound_effect() {
        return sound_effect;
    }
}
