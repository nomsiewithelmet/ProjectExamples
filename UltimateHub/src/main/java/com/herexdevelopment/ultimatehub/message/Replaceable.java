package com.herexdevelopment.ultimatehub.message;

import java.util.HashMap;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class Replaceable extends HashMap<String, Object> {
    public Replaceable() {
    }

    public Replaceable(String old, Object o) {
        this.put(old, o);
    }

    public Replaceable add(String old, Object o) {
        this.put(old, o);
        return this;
    }

}
