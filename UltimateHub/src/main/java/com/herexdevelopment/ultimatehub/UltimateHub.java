package com.herexdevelopment.ultimatehub;

import com.herexdevelopment.ultimatehub.command.command.BasicCommandManager;
import com.herexdevelopment.ultimatehub.listeners.CrateListener;
import com.herexdevelopment.ultimatehub.listeners.MenuListener;
import com.herexdevelopment.ultimatehub.listeners.PlayerListener;
import com.herexdevelopment.ultimatehub.lootbox.LootboxEvents;
import com.herexdevelopment.ultimatehub.lootbox.LootboxManager;
import com.herexdevelopment.ultimatehub.menu.ExecuteEvent;
import com.herexdevelopment.ultimatehub.menu.MenuDataManager;
import com.herexdevelopment.ultimatehub.particle.ParticleManager;
import com.herexdevelopment.ultimatehub.player.PlayerData;
import com.herexdevelopment.ultimatehub.scoreboard.ScoreBoard;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.parser.ParseException;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;
import com.herexdevelopment.ultimatehub.configmanager.json.JsonConfigManager;
import com.herexdevelopment.ultimatehub.utils.AdvancedLicense;

import java.io.File;
import java.io.IOException;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 28-4-18
 */
public class UltimateHub extends JavaPlugin {

    private static UltimateHub instance = null;

    private AdvancedLicense linces;
    private ScoreBoard scoreBoard;
    private LootboxManager lootboxManager;
    private MenuDataManager menuDataManager;


    public UltimateHub() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static UltimateHub getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }



    @Override
    public void onEnable() {
        instance = this;
        setupDirs();
        pretyPrint();
        regsterConfigs();
        new LootboxEvents();
        loadAll();
        registerListener();
        registerMenu();
        registerScoreboard();
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        new BasicCommandManager();
        registrerLootbox();
        checkAPI();
        registerLinces();
    }



    private void registerLinces(){
        linces = new AdvancedLicense(ConfigManager.getInstance().getConfig("config").getConfiguration().getString("key"), "https://linces.herex.pw/verify.php", this);
        linces.register();
    }

    private void pretyPrint(){
        System.out.println("----------------------------------------------");
        System.out.println();
        System.out.println(" ");
        System.out.println("                   UtlimateHub                 ");
        System.out.println("                     by Herex                  ");
        System.out.println("                     version: " + this.getDescription().getVersion() + "                   ");
        System.out.println();
        System.out.println();
        System.out.println("----------------------------------------------");

    }



    private void checkAPI(){
        if( Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            System.out.print("");
            System.out.print("");
            System.out.print("----------------------------------------------");
            System.out.print(" ");
            System.out.print("             PlaceholderAPI - FOUND           ");
            System.out.print(" ");
            System.out.print(" ");
            System.out.print("----------------------------------------------");

        }
        }




        private void registerScoreboard(){
        this.scoreBoard = new ScoreBoard();
        ScoreBoard.getInstance().recapBoard();
        }

        public void setupDirs(){
        File menudir = new File(this.getDataFolder().getAbsolutePath() + "/menus/");
        if(!menudir.exists()){
            menudir.mkdir();
        }


            File cratedir = new File(this.getDataFolder().getAbsolutePath() + "/crates/");
            if(!cratedir.exists()){
                cratedir.mkdir();
            }



            File plaerdir = new File(this.getDataFolder().getAbsolutePath() + "/players/");
            if(!plaerdir.exists()){
                plaerdir.mkdir();
            }
        }
    private void regsterConfigs(){
        ConfigManager.getInstance().registerConfigs();
        ConfigManager.getInstance().saveAllDefaults();
        new JsonConfigManager();
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new MenuListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new CrateListener(), this);

    }

    private void loadAll(){
        new PlayerData();
        this.menuDataManager = new MenuDataManager();
        MenuDataManager.getInstance().loadJsonFiles();
        try {
            MenuDataManager.getInstance().loadMenu();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        MenuDataManager.getInstance().loadJoinItems();
        new ParticleManager();
        ParticleManager.getInstance().loadALL();


    }

    private void registrerLootbox(){
        this.lootboxManager = new LootboxManager();
        try {
            LootboxManager.getInstance().load_lootbox();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void registerMenu(){
        new ExecuteEvent();

    }


    public ScoreBoard getScoreBoard() {
        return scoreBoard;
    }

    public LootboxManager getLootboxManager() {
        return lootboxManager;
    }


    public MenuDataManager getMenuDataManager() {
        return menuDataManager;
    }

    public AdvancedLicense getLinces() {
        return linces;
    }
}
