package com.herexdevelopment.ultimatehub.command.command;


import com.herexdevelopment.ultimatehub.command.commands.CrateCommands;
import com.herexdevelopment.ultimatehub.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import com.herexdevelopment.ultimatehub.UltimateHub;
import com.herexdevelopment.ultimatehub.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: PluginBase
 * Creation date: 17-12-2017
 * website: nomsiedev.ga
 */
public class BasicCommandManager implements CommandExecutor, TabCompleter {
    private ArrayList<BasicCommand> cmd = new ArrayList<BasicCommand>();

    public BasicCommandManager() {
        registerBasicCommands(new CrateCommands());
    }


    private void registerBasicCommands(BasicCommand acmd) {
        registerBasicCommand(acmd);
        UltimateHub.getInstance().getCommand(acmd.getCmd()).setExecutor(this);

    }


    private void registerBasicCommand(BasicCommand bc) {
        if (!cmd.contains(bc)) {
            cmd.add(bc);
        }

    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use commands ingame!");
            return true;
        }
        if (cmd.isEmpty()) {
            return true;
        }
        for (BasicCommand bc : cmd) {
            String[] argus = bc.getArs();

            if (!bc.isNeedargs() && args == null) {
                bc.run(sender, new String[]{});
                return true;
            }

            if (args.length > argus.length && bc.isNeedargs() && !bc.getArgMatch(args[0]).isNeedValue()) {
                sender.sendMessage(StringUtils.repColor(Message.TO_MANY_ARGUMENTS.toString()));
                return true;
            }


            if (!bc.allowRun(sender) && args.length > 0) {
                sender.sendMessage(StringUtils.repColor(Message.NO_PERMMISION.toString()));
                return true;
            }

            if (!bc.allowRun(sender)) {
                sender.sendMessage(StringUtils.repColor(Message.NO_PERMMISION.toString()));
                return true;
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("help")) {
                    sender.sendMessage(StringUtils.repColor("&cUsgae: &4/" + bc.getCmd()));
                    for (String ar : argus) {
                        sender.sendMessage(StringUtils.repColor("&8- &c/" + bc.getCmd() + " &7" + ar));
                    }
                    sender.sendMessage(StringUtils.repColor("&c "));
                    sender.sendMessage(StringUtils.repColor("&c " + bc.getDesc()));
                    return true;
                }
            }

            if (args.length == 2) {
                if (args[1].equalsIgnoreCase("help")) {
                    for (String ar : args) {
                        if (ar.equalsIgnoreCase(args[0])) {
                            sender.sendMessage(StringUtils.repColor("&cUsgae: &4/" + bc.getArgMatch(args[0]).getDesc()));
                            sender.sendMessage(StringUtils.repColor("&c "));
                            sender.sendMessage(StringUtils.repColor("&cArguments: &4" + bc.getArgMatch(args[0]).getArg()));
                            return true;
                        } else {
                            sender.sendMessage(StringUtils.repColor("&cWe dont found the argument: " + args[0]));
                            return true;

                        }
                    }
                }
            }


            if ((bc.getArgMatch(args[0]).allowRun(sender))) {
                if (!bc.getArgMatch(args[0]).isNeedValue() && !bc.isNeedValueWithSpaces() && args.length > 2) {
                    sender.sendMessage(StringUtils.repColor(Message.NO_SPACES_PERMISSON.toString()));
                    return true;
                }
            } else if (!bc.getArgMatch(args[0]).isNeedValue() && !bc.isNeedValueWithSpaces() && args.length == 2) {
                bc.run(sender, args);
                return true;
            } else if (bc.getArgMatch(args[0]).isNeedValue() && bc.isNeedValueWithSpaces() && args.length > 1) {
                bc.run(sender, args);
            return true;

    }else{
                sender.sendMessage(StringUtils.repColor(Message.TO_MANY_ARGUMENTS.toString()));
                return true;
            }

            if (args.length == 2 && !bc.getArgMatch(args[0]).isNeedValue()) {
                sender.sendMessage(StringUtils.repColor(Message.TO_MANY_ARGUMENTS.toString()));
                return true;
            }

            if (bc.getArgMatch(args[0]).hasArgs()) {
                bc.run(sender, args);
                return true;
            } else if (bc.getArgMatch(argus[0]).hasArgs()) {
                bc.run(sender, args);
                return true;
            } else
                bc.run(sender, new String[]{});
            return true;

        }


        return false;

    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        ArrayList<String> args = new ArrayList<>();
        for (BasicCommand bc : cmd) {
            if (command.getName().equalsIgnoreCase(bc.getCmd())) {
                Collections.addAll(args, bc.getArs());

            }
        }

        return args;

    }
}