package com.herexdevelopment.ultimatehub.command.command;

import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by: Naomi Beerenfenger
 * Project: PluginBase
 * Creation date: 17-12-2017
 * website: nomsiedev.ga
 */
public class BasicCommand {

    private String cmd, permission, desc;
    private boolean needargs;
    ArrayList<BasicArgument> argums;
    private boolean nameWithSpaces = false;


    /**
     * Constructor
     * Converts the Arraylit to String[]
     */
    String[] getArs() {
        String strNames[] = new String[argums.size()];
        for(int i=0; i < argums.size(); i++)
            strNames[i] = argums.get(i).getArg();
        return strNames;
    }

    String getCmd() {
        return cmd;
    }


    /**
     * Constructor
     * @param cmd Name of the command
     * @param desc The description of the plugin
     * @param permission Permission for the command
     */
    public BasicCommand(String cmd, String desc, String permission, boolean needargs){
        this.cmd = cmd;
        this.permission = permission;
        this.argums = new ArrayList<>();
        this.desc = desc;
        this.needargs = needargs;
    }




    public void needValueWithSpaced(Boolean nameWithSpaces){
        this.nameWithSpaces = nameWithSpaces;
    }


    public Boolean isNeedValueWithSpaces(){
        return this.nameWithSpaces;
    }


    public ArrayList<BasicArgument> getArgums() {
        return argums;
    }

    public void setArgums(ArrayList<BasicArgument> argums) {
        this.argums = argums;
    }

    /**
     * Constructor
     * @param sender Command sender
     * @param args The args of the sended commands
     */
    public void run(CommandSender sender, String[] args) {

    }

    /**
     * Constructor
     * @param cmd Command sender
     * @param permission The permission of the argument
     */
    protected void addArgs(String cmd, String permission, String dc, boolean needvalue){
        this.argums.add(new BasicArgument(cmd, permission, dc, needvalue));
    }

    /**
     * Constructor
     * @param s Command sender
     */
    boolean allowRun(CommandSender s){
        return s.hasPermission(permission);
    }

    /**
     * Constructor
     * @param arg Argument for checking
     */
    BasicArgument getArgMatch(String arg) {
        for (BasicArgument a : argums) {
            if (a.getArg().equalsIgnoreCase(arg)){
                return a;
            }
        }
        return null;
    }


    String getDesc() {
        return desc;
    }

    boolean isNeedargs() {
        return needargs;
    }

}
