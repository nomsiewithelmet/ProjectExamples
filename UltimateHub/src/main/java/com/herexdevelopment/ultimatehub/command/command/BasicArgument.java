package com.herexdevelopment.ultimatehub.command.command;

import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by: Naomi Beerenfenger
 * Project: PluginBase
 * Creation date: 18-12-2017
 * website: nomsiedev.ga
 */
public class BasicArgument extends ArrayList<BasicArgument> {

    private String arg;
    private String permission;
    private String desc;
    private boolean needValue;


    BasicArgument(String args, String perm, String dc, Boolean needval){
        this.arg = args;
        this.permission = perm;
        this.desc = dc;
        this.needValue = needval;
    }

    public boolean isNeedValue() {
        return needValue;
    }

    public void setNeedValue(boolean needValue) {
        this.needValue = needValue;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    String getArg() {
        return arg;
    }


    boolean allowRun(CommandSender s){
        return s.hasPermission(permission);

    }

    boolean hasArgs(){
        return arg != null;
    }
}
