package com.herexdevelopment.ultimatehub.menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 28-4-18
 */
public class UtlimateMenu {


    private  String title, displayname;
    private  int size, soundId;
    private List<InvItem> invItems;

    public UtlimateMenu(String title, String displayname, int size, int soundId, ArrayList<InvItem> invItems) {
        this.title = title;
        this.size = size;
        this.soundId = soundId;
        this.invItems = invItems;
        this.displayname = displayname;
    }


    public String getDisplayname() {
        return displayname;
    }

    public String getTitle() {
        return title;
    }

    public int getSize() {
        return size;
    }

    public List<InvItem> getInvItems() {
        return invItems;
    }

}
