package com.herexdevelopment.ultimatehub.configmanager;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.logging.Level;

public class ConfigManager {

    private static ConfigManager instance;

    public static ConfigManager getInstance() {
        if (instance == null)
            instance = new ConfigManager();
        return instance;
    }

    private HashMap<String, Config> configs;

    private ConfigManager() {
        configs = new HashMap<String, Config>();
    }

    /**
     * Add an config to the manager no .yml needed at the end!
     * @param configName the name for the config
     * @return returns it directly
     */
    private Config addConfig(String configName){

        if (configs.containsKey(configName)) {
            Bukkit.getServer().getLogger().log(Level.INFO, "config \"" + configName + ".yml\" already exists" );
            return configs.get(configName);
        }
        return configs.put(configName, new Config(configName));
    }

    /**
     * get config by name no .yml needed at the end!
     * @param configName name of config
     * @return Config with that name
     */
    public Config getConfig(String configName){
        if (configs.containsKey(configName))
            return configs.get(configName);
        else
            return null;
    }

    /**
     * get file configurations no .yml needed at the end!
     * @param configName name of the config
     * @return FileConfiguration of that file
     */
    public FileConfiguration getFileConfiguration(String configName){
        if (configs.containsKey(configName))
            return configs.get(configName).getConfiguration();
        else
            return null;
    }


    /**
     * reload all configs
     */
    public void reloadAll(){
        for (Config config : configs.values()){
            config.reloadConfig();
        }
    }

    /**
     * Save all configs
     */
    public void saveAll(){
        for (Config config : configs.values()){
            config.saveConfig();
        }
    }

    /**
     * Save all default configs without overriding the current files
     */
    public void saveAllDefaults(){
        saveAllDefaults(false);
    }

    /**
     * Save all default configs
     * @param override if you want to override the current file (this wil happen each startup)
     */
    public void saveAllDefaults(boolean override){
        for (Config config : configs.values()){
            config.saveDefaultConfig(override);
        }
    }

    public void registerConfigs() {
        addConfig("config");
        addConfig("items");
        addConfig("messages");
        addConfig("data");
        addConfig("particles");






    }
}
