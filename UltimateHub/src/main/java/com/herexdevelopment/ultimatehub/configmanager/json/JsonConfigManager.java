package com.herexdevelopment.ultimatehub.configmanager.json;

import org.bukkit.Bukkit;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class JsonConfigManager {
    public HashMap<String, JsonConfig> configs = new HashMap<>();
    private static JsonConfigManager instance;

    public JsonConfigManager(){
        instance = this;
        register("scoreboard", "{\"scoreboard\":{\"enable\":false,\"title\":\"test\",\"scores\":{\"score1\":{\"scorepos\":10,\"scorename\":\"this is a score\"},\"score2\":{\"scorepos\":9,\"scorename\":\"updated:? value\"},\"score3\":{\"scorepos\":8,\"scorename\":\"dafuq this works?\"}},\"animation\":{\"enable\":false,\"lines\":[\"T\",\"TE\",\"TES\",\"TEST\",\"TES\",\"TE\",\"T\"]}}}");

    }
    public JsonConfig getConfig(final String configName) {
        if (this.configs.containsKey(configName)) {
            return this.configs.get(configName);
        }
        return null;
    }



    public String FileToJson(File f){
        JSONParser parser = new JSONParser();
        try {
            return parser.parse(new FileReader(f)).toString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JsonConfig getJsonByName(String name){
        for(Map.Entry<String, JsonConfig> conf : configs.entrySet()){
            if(conf.getKey().replace(".json", "").equalsIgnoreCase(name)){
                return conf.getValue();
            }
        }
        return null;
    }

    public void addConfig(final String configName, String json) {
        if (this.configs.containsKey(configName)) {
            Bukkit.getServer().getLogger().log(Level.INFO, "jsonconfig \"" + configName + ".json\" already exists");
        }
        this.configs.put(configName, new JsonConfig(configName, json));
        saveDefaultConfig(configName, json);
    }

    public void register(String name, String json){
        addConfig(name, json);
        saveDefaultConfig(name, json);

    }

    public void saveDefaultConfig(String name, String json) {
        for(Map.Entry<String, JsonConfig> g : this.configs.entrySet()){
            if(g.getKey().equalsIgnoreCase(name)){
                new JsonConfig(name, json);
            }

        }
    }



    public static JsonConfigManager getInstance() {
        return instance;
    }
}
