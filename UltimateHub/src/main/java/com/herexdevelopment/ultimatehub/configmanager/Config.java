package com.herexdevelopment.ultimatehub.configmanager;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import com.herexdevelopment.ultimatehub.UltimateHub;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class Config {

    private String fileName;
    private File configFile;
    private FileConfiguration fileConfiguration;

    public Config(String name){
        this(name, UltimateHub.getInstance().getDataFolder());
    }

    public Config(String name, File folder){
        if (UltimateHub.getInstance() == null)
            throw  new IllegalArgumentException("plugin cannot be null!");
        this.fileName = name + ".yml";
        if (folder == null)
            throw new IllegalStateException();
        this.configFile = new File(folder, fileName);
    }

    public void reloadConfig(){
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

        InputStream defConfigStream = UltimateHub.getInstance().getResource(fileName);
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
            fileConfiguration.setDefaults(defConfig);
        }
    }

    public FileConfiguration getConfiguration(){
        if (fileConfiguration == null)
            reloadConfig();
        return fileConfiguration;
    }

    public void saveConfig(){
        if (fileConfiguration != null && configFile != null)
            try {
                getConfiguration().save(configFile);
            }catch (IOException ex){
                UltimateHub.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
            }
    }
    
    
    void saveDefaultConfig(){
        saveDefaultConfig(false);
    }

    void saveDefaultConfig(boolean override){
        if (!configFile.exists())
            UltimateHub.getInstance().saveResource(fileName, false);
    }
}